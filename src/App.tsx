import React, { useState, useEffect } from 'react';
import './App.css';
import axios, { CancelTokenSource } from 'axios';
import '@fontsource/roboto';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Slider from '@material-ui/core/Slider';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CircularProgress from '@material-ui/core/CircularProgress';

interface IPartner {
  id: number,
  organization: string,
  website: string,
  distance: number,
  office: {
    location: string,
    address: string
  }
}

const defaultPartners:IPartner[] = [];

function App() {
  const [partners, setPartners]: [IPartner[], (partners: IPartner[]) => void] = React.useState(defaultPartners);
  const [loading, setLoading]: [boolean, (loading: boolean) => void] = React.useState<boolean>(true);
  const [range, setRange] = useState(10);

  const useStyles = makeStyles({
    root: {
      minWidth: 275,
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
  });

  const classes = useStyles();

  React.useEffect(() => {
    loadPartnersInRange(range);
  }, []);

  function loadPartnersInRange(range: number) {
    axios
      .get<IPartner[]>('http://localhost:8080/fetch-partners-in-range?range='+range, {
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Credentials':true
        },
        timeout: 10000,
      })
      .then((response) => {
        setPartners(response.data);
        setLoading(false);
      })
      .catch((ex) => {
        setLoading(false);
      });
  }

  function valuetext(value: number) {
    return `${value} KM`;
  }

  return (
    <div className="App">
      <div className={classes.root}>
        <Typography id="discrete-slider" gutterBottom>
          Partners in Range: {range} km
        </Typography>
        <Slider
          defaultValue={10}
          getAriaValueText={valuetext}
          aria-labelledby="discrete-slider"
          valueLabelDisplay="auto"
          step={10}
          marks
          min={0}
          max={500}
          onChangeCommitted={(_, value) => {
            setRange(+value);
            loadPartnersInRange(+value);
            setLoading(true);
          }
            
         }
        />
      </div>
      {loading && <CircularProgress /> }

      {
        partners.map((partner) => (
          <Card className={classes.root} variant="outlined">
            <CardContent>
              <Typography variant="h5" component="h2">
                {partner.organization}
              </Typography>
              <Typography className={classes.title} color="textSecondary" gutterBottom>
                ID: {partner.id}
              </Typography>
              <Typography className={classes.pos} component="h3">
                Nearest Office Distance: {partner.distance} KM - Location: {partner.office.location} - Address: {partner.office.address}
              </Typography>
            </CardContent>
          </Card>
        ))
      }
    </div>
  );
}

export default App;
