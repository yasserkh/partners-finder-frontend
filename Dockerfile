FROM node:14

# set your working directory  
WORKDIR /src

# add `/app/node_modules/.bin` to $PATH  
ENV PATH /node_modules/.bin:$PATH  

# install application dependencies  
COPY package.json ./  
COPY package-lock.json ./  
RUN npm install --silent  
RUN npm install react-scripts
 
# add app  
COPY . ./  
 
EXPOSE 3000

# will start app  
CMD ["npm", "start"] 